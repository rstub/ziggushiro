#define R_NO_REMAP
#include <stdlib.h> // for NULL
#include <R_ext/Rdynload.h>
#include <R_ext/Random.h>

static const R_CMethodDef CEntries[] = {
  {"user_unif_rand", (DL_FUNC) &user_unif_rand, 0, NULL},
  {"user_unif_init", (DL_FUNC) &user_unif_init, 0, NULL},
  {"user_unif_nseed", (DL_FUNC) &user_unif_nseed, 0, NULL},
  {"user_unif_seedloc", (DL_FUNC) &user_unif_seedloc, 0, NULL},
  {"user_norm_rand", (DL_FUNC) &user_norm_rand, 0, NULL},
  {NULL, NULL, 0, NULL}
};

static const R_CallMethodDef CallEntries[] = {
  {NULL, NULL, 0}
};

void R_init_addr(DllInfo *dll) {
  R_registerRoutines(dll, CEntries, CallEntries, NULL, NULL);
  R_useDynamicSymbols(dll, FALSE);
}
