// MIT License
//
// Copyright (c) 2023 Ralf Stubner
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
//   The above copyright notice and this permission notice shall be included in all
//   copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#define R_NO_REMAP
#include <R_ext/Random.h>
#include "xoshiro256plusplus.h"

// 256 bits of state
static int nseed = 8;
int * user_unif_nseed(void) { return &nseed; }
int * user_unif_seedloc(void) { return (int *) &s; }

void  user_unif_init(Int32 seed) {
  int *p = (int *) &s;
  for (int i = 0; i < nseed; i++) {
    seed = (69069 * seed + 1);
    p[i] = seed;
  }
}

double * user_unif_rand(void) {
  static double res;
  res = (next() >> 11) * 0x1.0p-53;
  return &res;
}
