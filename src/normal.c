// MIT License
//
// Copyright (c) 2023 Ralf Stubner
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
//   The above copyright notice and this permission notice shall be included in all
//   copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#define R_NO_REMAP
#include <math.h>
#include <R_ext/Random.h>
#include "normal_layers.h"

// This implementation is based on https://github.com/cd-mcfarland/fast_prng with
// some ideas from https://github.com/apache/commons-rng/blob/master/commons-rng-sampling/src/main/java/org/apache/commons/rng/sampling/distribution/ZigguratSampler.java.
// So far only "simple overhangs" are implemented.
// Major difference: The different tables are not scaled with 2^63 (or similar),
// since R's RNG interface does not give access to the raw output from the RNG.
// Instead `unif_rand()` and `R_unif_index()` are used. In the most common case,
// this means two draws from the RNG, even though a single draw would provide
// enough randomness.


// select a region using alias sampling
uint_fast8_t select_region(uint_fast8_t j) {
  double x = unif_rand();
  return x >= PMF[j] ? MAP[j] : j;
}

// Linear interpolation of v[] between j and j-1
double interpolate(double *v, int j, double u) {
  return v[j] + u * (v[j - 1] - v[j]);
}

double edge_sample(uint_fast8_t bits, double sign, double u) {
  uint_fast8_t j = select_region(bits);
  double U_1 = u;
  double x;
  if (j == 0) { // tail
    do
      x = ONE_OVER_X_0 * exp_rand();
    while (exp_rand() < 0.5*x*x);
    x += X_0;
  } else { // simple overhangs
    for (;;) {
      x = interpolate(X, j, U_1);
      if ( interpolate(Y, j, unif_rand()) < exp(-0.5*x*x) ) break;
      U_1 = unif_rand();
    }
  }
  return sign * x;
}

double * user_norm_rand(void) {
  static double res;
  uint_fast16_t bits = R_unif_index(65535);
  uint_fast8_t i = bits & 0xff;
  if (i < I_MAX) {
    double sign = (bits & 0x100) ? 1. : -1.;
    res =  X[i] * sign * unif_rand();
  } else {
    bits = R_unif_index(65535);
    double sign = (bits & 0x100) ? 1. : -1.;
    res = edge_sample(bits & 0xff, sign, unif_rand());
  }
  return &res;
}
